#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import regex as re
from collections import Counter

AFM_RE = re.compile(ur'(?P<all>\w\.?\w\.?\w *[:.,]? *(?P<target>\d{9}))')
DATE_RE = re.compile(ur'(?P<target>((\d{1,2}[-/]\d{1,2}[-/]\d{1,4})|(\d{1,2} +[Α-Ωα-ωάίέόύ]+ \d{2,4})))')
PRICES_RE = re.compile(ur'(?P<all>(?P<target>[0-9O]+[.,][0-9O](?=[^%])))')
TAX_RE = re.compile(ur'(?P<all>(?P<target>\d3([.,]\d{2})?%))')
TIME_RE = re.compile(ur'(?P<all>(?P<target>\d{2}[:.]\d{2}[:.]\d{2}))')
TOTAL_RE = re.compile(ur'(?P<all>.{6} [^0-9]* *(?P<target>/d{1,}[,.]?/d{2}))', re.MULTILINE)
AM_RE = re.compile(ur'(?P<all> (?P<target>[A-ZΑ-Ω0]{2,3} *\d{8}))')
AA_RE = re.compile(ur'(?P<all> (?P<target>\d{6}))')

PATTERNS = {
            'afm': lambda text: find_occurence(text, AFM_RE),
            'date': lambda text: find_occurence(text, DATE_RE),
            # 'prices': lambda text: sorted([float(match.replace(',', '.').replace('O','0'))
            #                                for match in
            #                                find_all(text, PRICES_RE)]),
            'tax': lambda text: Counter([match.replace(',', '.')
                                        for match in find_all(text, TAX_RE)]),
            'time': lambda text: find_occurence(text, TIME_RE),
            'total': lambda text: find_occurence(text, TOTAL_RE),
            'am': lambda text: find_occurence(text, AM_RE),
            'aa': lambda text: find_occurence(text, AA_RE),
           }


OUTFILE = '/var/www/html/out.txt'

def find_occurence(text, regex, reverse=False):
    try:
        return regex.finditer(text).next()['target']
    except StopIteration:
        return 'null'


def find_all(text, regex):
    vals = list(match['target'] for match in regex.finditer(text))
    return vals or ['none']


if __name__ == '__main__':
    attributes = {}
    in_file = sys.argv[1]
    with open(in_file) as receipt:
        text = receipt.read().decode('utf-8')
        # print(text)
        # attributes['company'] = text.split('\n')[5]
        for label, pattern in PATTERNS.items():
            attributes[label] = pattern(text)
    with open(OUTFILE, 'w') as out:
        json.dump(attributes, out)
